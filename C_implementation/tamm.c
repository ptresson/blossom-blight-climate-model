//Same tamm model but coded in C
#include <stdio.h>
#include <math.h>

 
/* function declaration */
float tamm(float par[], float temp, float prec);

int main () {

   /* a float array with 8 elements */
   float param[8] = {0, 31, 0.9, 0.79, 1.70951, 1.47221, 1.5751, 1.9652};
   float inc;
   float temperature = 15.0;
   float precipitations = 20.0;

   /* pass pointer to the array as an argument */
   inc = tamm(param, temperature, precipitations) ;
 
   /* output the returned value */
   printf( "Incidence value is: %f \n", inc );
    
   return 0;
}



float tamm(float par[], float temp, float prec) {

   float incidence;

   float Tmin  = par[0];
   float Tmax  = par[1];
   float m     = par[2];
   float imax  = par[3];
   float gamma1= par[4];
   float gamma2= par[5];
   float ro1   = par[6];
   float ro2   = par[7];

   float phi=(temp-Tmin)/(Tmax-Tmin);
   float interm_i0 = powf(phi,gamma2);
   float interm_r  = powf(phi,ro2);
   float i0 = gamma1 * interm_i0 * (1-phi);
   float r  = ro1 * interm_r * (1-phi);
   float term1 = 1 - powf(i0,(1-m));
   float term2 = exp(-r*prec);

   incidence = imax * powf((1 - term1 * term2),(1/(1-m)));

   return incidence;
}
