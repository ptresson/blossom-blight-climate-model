Infec.Tamm2=function(par, temp, swd)
  
# fonction de calcul du %age de fleurs de cerisier moniliees (monilia laxa) 
# etablie par Tamm et al. (1995) a l'aide de mesures en chambres climatiques (in vitro)
#ajout d'un facteur pluvio/(pluvio+E) pour forcer le modèle à 0 pour pluvio = 0
{

# valeurs obtenues par Tamm et al., 1995: Tmin=0°C, Tmax=31°C, m=0.9, 
# imax=0.874, gamma1=1.816, gamma2=1.736, ro1=0.728, ro2=0.918
# paramètres obtenus avec optimisation cette année : Imax = 0.79, gamma1 = 1.70951, gama2 = 1.47221, ro1 = 1.5751, ro2 = 1.9652
# par <- c(0, 31, 0.9, 0.79, 1.709, 1.4722, 1.5751, 1.9652, 0.029) 

  Tmin  = par[1]
  Tmax  = par[2]
  m     = par[3]
  imax  = par[4]
  gamma1= par[5]
  gamma2= par[6]
  ro1   = par[7]
  ro2   = par[8]
  E     = par[9]

  
  
# calcul de phi en fonction des valeurs de temp
  phi<-NULL
  for(i in 1:length(temp)) {
    phi[i]=(temp[i]-Tmin)/(Tmax-Tmin)
  }

# calcul de i0 et r en fonction des valeurs de phi(T)
  i0<-NULL
  r<-NULL
  interm.i0<-NULL
  interm.r<-NULL
  
  for(i in 1:length(temp)) {
    interm.i0[i] = phi[i]^gamma2
    interm.r[i] = phi[i]^ro2
    i0[i]=gamma1*interm.i0[i]*(1-phi[i])
    r[i]= ro1*interm.r[i]*(1-phi[i])
  }
  

# calcul des valeurs d incidence d infection, infec.e, en fonction de asymptote (qui varie selon la temperature temp) et de swd (duree d'humectation)
term1<-NULL
term2<-NULL
infec.e<-NULL

for(i in 1:length(temp)) {
 term1 = 1-i0[i]^(1-m)
 term2 = exp(-r[i]*swd[i])
 infec.e[i] = (swd[i]/(swd[i]+E))*imax*(1-term1*term2)^(1/(1-m))
}

return(infec.e)

}
